'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Penulismvc extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Penulismvc.hasMany(models.Bukumvc, {foreignKey: 'penulis_id',constraints: true })
    }
  };

  Penulismvc.init({
    nama: DataTypes.STRING,
    email: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Penulismvc',
  });
  return Penulismvc;
};