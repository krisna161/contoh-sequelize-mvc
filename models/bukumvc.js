'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Bukumvc extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Bukumvc.belongsTo(models.Penulismvc, {foreignKey: 'penulis_id',constraints: true })
    }
  };
  Bukumvc.init({
    judul: DataTypes.STRING,
    deskripsi: DataTypes.STRING,
    penulis_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Bukumvc',
  });
  return Bukumvc;
};