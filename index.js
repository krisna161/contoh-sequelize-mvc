const express = require("express");
const indexController = require("./controllers/indexController");

const app = express();
const port = process.env.PORT || 3000;

app.use(express.json());

//Endpoint Book
app.get('/books', function(req, res) {
    try{
        indexController.getAllBook(req, res, function(error, user){
            if (!error){
                res.status(200).json({
                    status: true,
                    message: 'OK',
                    user
                })
            } else {
                res.status(202).json({
                    status: false,
                    message: 'Error'
                })
            }
        })
    }
    catch(err){
        res.status(422).json({
        status: false,
        message: err.message
        })
    }
})

app.get('/books/:id', function(req, res) {
    try{
        indexController.getBookById(req, res, function(error, user){
            if (!error){
                res.status(200).json({
                    status: true,
                    message: 'OK',
                    user
                })
            } else {
                res.status(202).json({
                    status: false,
                    message: 'Error'
                })
            }
        })
    }
    catch(err){
        res.status(422).json({
        status: false,
        message: err.message
        })
    }
})
app.post('/books', function(req, res) {
    try{
        indexController.createOneBook(req, res, function(error, user){
            if (!error){
                res.status(200).json({
                    status: true,
                    message: 'OK',
                    user
                })
            } else {
                res.status(202).json({
                    status: false,
                    message: 'Error'
                })
            }
        })
    }
    catch(err){
        res.status(422).json({
        status: false,
        message: err.message
        })
    }
})
app.put('/books/:id', function(req, res) {
    try{
        indexController.updateBook(req, res, function(error, user){
            if (!error){
                res.status(200).json({
                    status: true,
                    message: 'OK',
                    user
                })
            } else {
                res.status(202).json({
                    status: false,
                    message: 'Error'
                })
            }
        })
    }
    catch(err){
        res.status(422).json({
        status: false,
        message: err.message
        })
    }
})

app.delete('/books/:id', function(req, res) {
    try{
        indexController.deleteOneBook(req, res, function(error, user){
            if (!error){
                res.status(200).json({
                    status: true,
                    message: 'OK',
                    user
                })
            } else {
                res.status(202).json({
                    status: false,
                    message: 'Error'
                })
            }
        })
    }
    catch(err){
        res.status(422).json({
        status: false,
        message: err.message
        })
    }
})

//Endpoint Author
app.get('/authors', function(req, res) {
    try{
        indexController.getAllAuthor(req, res, function(error, user){
            if (!error){
                res.status(200).json({
                    status: true,
                    message: 'OK',
                    user
                })
            } else {
                res.status(202).json({
                    status: false,
                    message: 'Error'
                })
            }
        })
    }
    catch(err){
        res.status(422).json({
        status: false,
        message: err.message
        })
    }
})

app.get('/authors/:id', function(req, res) {
    try{
        indexController.getAuthorByIdWithBook(req, res, function(error, user){
            if (!error){
                res.status(200).json({
                    status: true,
                    message: 'OK',
                    user
                })
            } else {
                res.status(202).json({
                    status: false,
                    message: 'Error'
                })
            }
        })
    }
    catch(err){
        res.status(422).json({
        status: false,
        message: err.message
        })
    }
})
app.post('/authors', function(req, res) {
    try{
        indexController.createOneAuthor(req, res, function(error, user){
            if (!error){
                res.status(200).json({
                    status: true,
                    message: 'OK',
                    user
                })
            } else {
                res.status(202).json({
                    status: false,
                    message: 'Error'
                })
            }
        })
    }
    catch(err){
        res.status(422).json({
        status: false,
        message: err.message
        })
    }
})
app.put('/authors/:id', function(req, res) {
    try{
        indexController.updateAuthor(req, res, function(error, user){
            if (!error){
                res.status(200).json({
                    status: true,
                    message: 'OK',
                    user
                })
            } else {
                res.status(202).json({
                    status: false,
                    message: 'Error'
                })
            }
        })
    }
    catch(err){
        res.status(422).json({
        status: false,
        message: err.message
        })
    }
})

app.delete('/authors/:id', function(req, res) {
    try{
        indexController.deleteOneAuthor(req, res, function(error, user){
            if (!error){
                res.status(200).json({
                    status: true,
                    message: 'OK',
                    user
                })
            } else {
                res.status(202).json({
                    status: false,
                    message: 'Error'
                })
            }
        })
    }
    catch(err){
        res.status(422).json({
        status: false,
        message: err.message
        })
    }
})

app.listen(port, () => console.log(`Listen on port ${port}`));
