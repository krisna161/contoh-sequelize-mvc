exports.landingPage = (req,res,next) => {
    res.render('index',{title: 'Telkomsel'});
}

const { Bukumvc,Penulismvc } = require('../models');
const penulismvc = require('../models/penulismvc');

//controller Book
function getAllBook(req, res, next){
    try{
        Bukumvc.findAll({order: [ [ 'id', 'ASC' ]]}).then(user => {
            res.status(200).json({
                status: true,
                message: 'Book retreived',
                user
            })
        })
    }
    catch(err){
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
}
function getBookById(req, res, next){
    try{
        Bukumvc.findByPk(req.params.id).then(user => {
            if (user === null) {
                res.status(202).json({
                    status: false,
                    message: `User with ID ${req.params.id} not Found!`,
                    user
                })
            } else {
                res.status(200).json({
                    status: false,
                    message: `User with ID ${req.params.id} retrieved!`,
                    user
                })
            }
        })
    }
    catch(err){
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
}

function createOneBook(req,res,next) {
    try{
        Bukumvc.findOne({ 
            where: {judul: req.body.judul},
            order: [ [ 'id', 'ASC' ]],
            }).then(user => {
            if (user){
                res.status(422).json({
                    status: false,
                    message: `Create user failed due to ${req.body.judul} already exist`
                })
            } else {
                Bukumvc.create({
                    judul: req.body.judul,
                    deskripsi: req.body.deskripsi,
                    penulis_id: req.body.penulis_id
                }).then(user => {
                    res.status(201).json({
                        status: true,
                        message: `User with ID ${req.body.id} created`,
                        user
                    })
                })
            }
        })
    } 
    catch(err) {
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
}

function updateBook(req, res, next){
   try {
    Bukumvc.findByPk(req.params.id).then(user => {
        if (user === null) {
            res.status(200).json({
                status: false,
                message: `User ID ${req.params.id} not found`,
                user
            })
        } else {
            Bukumvc.update({
                judul: req.body.judul,
                deskripsi: req.body.deskripsi,
                penulis_id: req.body.penulis_id
            }, {
                where: {
                    id: req.params.id
                }
            }).then(user => {
                res.status(200).json({
                    status: true,
                    message: `User with ID ${req.params.id} updated!`,
                    user
                })
            }) 
        }
    })
}
catch(err){
    res.status(422).json({
      status: false,
      message: err.message
  })
}
}
 
function deleteOneBook(req, res, next){
    try{
        Bukumvc.findOne({ where: {id: req.params.id}}).then(user => {
            if (user === null){
                res.status(201).json({
                    status: false,
                    message: `User ID ${req.params.id} does not exist`
                })
            } else {
                Bukumvc.destroy({ where: {id: req.params.id}}).then(user => {
                    res.status(200).json({
                        status: true,
                        message: `User ID ${req.params.id} deleted`
                    })
                })
            }
           })
    }
    catch(err){
        res.status(422).json({
          status: false,
          message: err.message
      })
    }
}

//controller Author
function getAllAuthor(req, res, next){
    try{
        Penulismvc.findAll().then(user => {
            res.status(200).json({
                status: true,
                message: 'Book retreived',
                user
            })
        })
    }
    catch(err){
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
}

async function getAuthorByIdWithBook(req,res,next) {
    try {
        const user = await Penulismvc.findOne({
            where: { id: req.params.id },
            include: [{
            model: Bukumvc
            }]
            });
            if (user === null) {
                res.status(202).json({
                    status: false,
                    message: `User with ID ${req.params.id} not Found!`,
                    user
                })
            } else {
                res.status(200).json({
                    status: false,
                    message: `User with ID ${req.params.id} retrieved!`,
                    user
                })
            }
    } catch (err){
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
}

function getAuthorById(req, res, next){
    try{
        Penulismvc.findByPk(req.params.id).then(user => {
            if (user === null) {
                res.status(202).json({
                    status: false,
                    message: `User with ID ${req.params.id} not Found!`,
                    user
                })
            } else {
                res.status(200).json({
                    status: false,
                    message: `User with ID ${req.params.id} retrieved!`,
                    user
                })
            }
        })
    }
    catch(err){
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
}

function createOneAuthor(req,res,next) {
    try{
        Penulismvc.findOne({ where: {email: req.body.email}}).then(user => {
            if (user){
                res.status(201).json({
                    status: false,
                    message: `Create user failed due to ${req.body.email} already exist`
                })
            } else {
                Penulismvc.create({
                    nama: req.body.nama,
                    email: req.body.email
                }).then(user => {
                    res.status(201).json({
                        status: true,
                        message: `User with ID ${req.body.id} created`,
                        user
                    })
                })
            }
        })
    } 
    catch(err) {
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
}

function updateAuthor(req, res, next){
   try {
    Penulismvc.findByPk(req.params.id).then(user => {
        if (user === null) {
            res.status(200).json({
                status: false,
                message: `User ID ${req.params.id} not found`,
                user
            })
        } else {
            Penulismvc.update({
                nama: req.body.nama,
                email: req.body.email
            }, {
                where: {
                    id: req.params.id
                }
            }).then(user => {
                res.status(200).json({
                    status: true,
                    message: `User with ID ${req.params.id} updated!`,
                    user
                })
            }) 
        }
    })
}
catch(err){
    res.status(422).json({
      status: false,
      message: err.message
  })
}
}
 
function deleteOneAuthor(req, res, next){
    try{
        // Bukumvc.findOne({ where: {  penulis_id: Penulismvc.id } }).then (Book => {
        //     if (!null) {
        //         res.status(201).json({
        //             status: false,
        //             message: `Canot deleted user ID ${req.params.id} due to has books`
        //         })
        //     }
        // } )
        Penulismvc.findOne({ where: {id: req.params.id}}).then(user => {
            if (user === null){
                res.status(201).json({
                    status: false,
                    message: `User ID ${req.params.id} does not exist`
                })
            } else {
                Penulismvc.destroy({ where: {id: req.params.id}}).then(user => {
                    res.status(200).json({
                        status: true,
                        message: `User ID ${req.params.id} deleted`
                    })
                })
            }
           })
    }
    catch(err){
        res.status(422).json({
          status: false,
          message: err.message
      })
    }
}



module.exports = {
    getAllBook,
    getBookById,
    updateBook,
    deleteOneBook,
    kcreateOneBoo,
    getAllAuthor,
    getAuthorById,
    updateAuthor,
    deleteOneAuthor,
    createOneAuthor,
    getAuthorByIdWithBook
}